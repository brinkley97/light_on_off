//
//  ViewController.swift
//  Light
//
//  Created by Detravious J. Brinkley on 2/10/20.
//  Copyright © 2020 Detravious J. Brinkley. All rights reserved.
//

import UIKit

var lightOn = true
class ViewController: UIViewController {
   
    
    @IBOutlet var newColor: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//change text color of button
        newColor.setTitleColor(.red, for: .normal)
        
    }

    
    @IBAction func buttonClicked(_ sender: Any) {
        print("The button was pressed")
        lightOn.toggle()
        onOff()
    }
    
    func onOff() {
        //toggle() = change from true to false
        if lightOn{
            view.backgroundColor = .white
        }else{
            view.backgroundColor = .black
        }
    }
        
   
   
    
}

