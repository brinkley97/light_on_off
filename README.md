# Light Project

Special Topics: iOS Development Class at Claflin University

## Features
- Change background color with button

---
## Contributors
* Detravious Brinkley <d.brinkley97@gmail.com>

---
## Run Code
1. Clone Repo
2. Open xcode

---
## Resources
- App Development With Swift book in app store